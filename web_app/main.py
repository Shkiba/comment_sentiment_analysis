import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from keras import backend as K
from scipy.stats import uniform, randint
from sklearn.datasets import load_breast_cancer, load_diabetes, load_wine
from sklearn.metrics import auc, accuracy_score, confusion_matrix, mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV, KFold, RandomizedSearchCV, train_test_split
import xgboost as xgb
import streamlit as st
import pickle


def load_my_model():
    model = load_model('model.hdf5')
    model.summary()

    return model

def load_tokenizer():
    with open('tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    return tokenizer

if __name__ == '__main__':
    st.title('My first app')
    sentence = st.text_input('Input your sentence here:')
    model = load_my_model()
    if sentence:
        testing_seq = tokenizer.texts_to_sequences(sentence)
        testing_pad = pad_sequences(testing_seq, maxlen=100)
        y_hat = model.predict(sentence)
        if y_hat > 0.5:
            st.text("Happy")
        else:
            st.text("Sad")